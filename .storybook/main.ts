import type { StorybookConfig } from "@storybook/react/types";

const config: StorybookConfig = {
  stories: [
    {
      directory: "../src",
      titlePrefix: "Demo"
    }
  ],

  logLevel: "debug",

  addons: [
    "@storybook/addon-essentials",
    "@storybook/addon-a11y",
    "@storybook/addon-jest"
  ],

  typescript: {
    check: true
  },

  core: {
    builder: "webpack5"
  },

  features: {
    postcss: false,
    // modernInlineRender: true,
    storyStoreV7: !global.navigator?.userAgent?.match?.("jsdom"),
    buildStoriesJson: true,
    babelModeV7: true,
    warnOnLegacyHierarchySeparator: false
  },

  framework: "@storybook/react"
};

module.exports = config;
