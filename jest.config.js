/**
 * @type {import("ts-jest/dist/types").InitialOptionsTsJest}
 */
module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",

  // It suggests that an array of regex expression sample strings,
  // matched against all module paths before considered "visible" to the module loader
  modulePathIgnorePatterns: ["node_modules", "jest-test-results.json"]
};
